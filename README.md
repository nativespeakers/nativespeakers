
# Nativespeakers readme

This is an app that allows native speakers living in foreign countries advertise their teaching skills.
 
It's a supercharged http://nativespeaker.com.pl written from scratch, which will eventually replace the original.

## Ruby version
App runs on`ruby-2.3.0`but the code is so generic it could probably run on any other version as long as `Rails 4.2.x` is supported

## Dependencies
Any Linux box is suitable.
Mailing depends on Sendgrid. Geocoding depends on Google. Storage relies on FTP.

## Configuration

###Secret values
Secret values are stored at `config/application.yml` and are handled by `figaro` gem to bring them to environment variables. Minimum configuration:
```
google_api_key: "xxx"
sendgrid_username: "xxxx"
sendgrid_password: "xxx"
sendgrid_apikey: "xxx"
ftp_host: "xxx"
ftp_user: "xxx"
ftp_password: "xxx"
```
### Services 
App doesn't rely on any delayed jobs. There is an optional functionality to send e-mail notifications to user who don't check their messages. It's up to you how often you do it and what method you use it to call `rake background:send_reminders`.