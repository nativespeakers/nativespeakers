class ProfilePolicy < ApplicationPolicy
  def index?
    user.admin? or user.moderator?
  end

  def update?
    user.admin? or (record.user_id == user.id) or (user.moderator? and !record.user.admin)
  end

  def destroy?
    user.admin? or (record.user_id == user.id)
  end

  def feature?
    Profile.where('featured_until >= ?', Time.now).count < 3 && (record.featured_until.blank? ? true : record.featured_until < Time.now)
  end

  def approve?
  	(user.admin? or user.moderator?) and !record.approved?
  end

  def reject?
    (user.admin? or user.moderator?) and !record.rejected?
  end

  def dashboard?
  	(user.admin? or user.moderator?)
  end

  def upgrade?
    (user.admin? or user.moderator?)
  end
end
