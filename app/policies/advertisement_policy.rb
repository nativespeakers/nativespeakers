class AdvertisementPolicy < ApplicationPolicy
  def update?
    user.present? && (user.admin? or user.moderator?)
  end

  def edit?
    user.present? && (user.admin? or user.moderator?)
  end

  def destroy?
    user.admin?
  end
end
