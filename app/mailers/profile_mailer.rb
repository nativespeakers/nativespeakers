class ProfileMailer < ApplicationMailer
  def reminder_mail(profile)
    @profile = profile
    @number_of_messages = @profile.waiting_messages

    mail(to: @profile.user.email, from: "\"NativeSpeaker\" <notifications@nativespeaker.com.pl>", subject: "New message on NativeSpeakers.com.pl")
    @profile.last_reminder_sent_at = Time.now
    @profile.reminders_sent += 1
    @profile.save
  end

  def rejected_mail(profile)
    @profile = profile
    mail(to: @profile.user.email, from: "\"NativeSpeaker\" <notifications@nativespeaker.com.pl>", subject: "Your native speaker profile was rejected")
  end

  def approved_mail(profile)
    @profile = profile
    mail(to: @profile.user.email, from: "\"NativeSpeaker\" <notifications@nativespeaker.com.pl>", subject: "Your native speaker profile was approved!")
  end

  def request_feedback_mail(profile)
    @profile = profile
    mail(to: @profile.user.email, from: "\"NativeSpeaker\" <notifications@nativespeaker.com.pl>", subject: "Does NativeSpeaker work for you? 🤞")
  end

  def payment_received_mail(profile)
  	@profile = profile
    mail(to: @profile.user.email, from: "\"NativeSpeaker\" <notifications@nativespeaker.com.pl>", subject: "We've just received your 💸 (money)")
  end

  def waiting_for_approval_mail(profile)
    @profile = profile
    mail(to: 'nativespeaker.com.pl@gmail.com', from: "\"NativeSpeaker\" <notifications@nativespeaker.com.pl>", subject: "Waiting for approval for #{@profile.name}")
  end
end
