class ApplicationMailer < ActionMailer::Base
  default from: "\"NativeSpeaker.com.pl\" <nativespeaker.com.pl@gmail.com>"

  layout 'mailer'
end
