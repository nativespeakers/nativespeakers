class StaticPagesController < ApplicationController
  def home
    @languages = Language.all
    @age_groups = AgeGroup.all
    @featured_profiles = @current_domain.profiles.where("featured_until >= ?", Time.now).where(:featured_visible => true)
    @profiles = @current_domain.profiles.where(status: Profile.statuses[:approved]).where("premium_until > ?", Time.now).joins(:user).order('users.last_active_at DESC')
    # this shows non-premium profiles
    # @profiles = @profiles + @current_domain.profiles.where(status: Profile.statuses[:approved]).where("premium_until < ? OR premium_until IS NULL", Time.now).order(created_at: :desc)
  end

  def tos
  end

  def about
  end

  def cookie_policy
  end

  def help
  end

  def too_many_messages
  end
end
