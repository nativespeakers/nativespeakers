class MessagesController < ApplicationController
	def create
    blocked_emails = ['k5@thetower.com.pl', 'k3@thetower.com.pl', 'sekretariat@thetower.com.pl', 'rekrutacja@sympozjum.edu.pl', 'smyczeks@wp.pl', 'cosmopolitan@cosmopolitan.edu.pl', 'pelka@beclever.edu.pl', 'info@languagelevels.pl', 'Saturewa@gmail.com', 'bytom@level.edu.pl', 'rekrutacja@eklektika.pl', 'a.kozimor@linguanova.com.pl', 'katarzyna.gapinska@cta.ai', 'info@epc.edu.pl', 'admissions@epc.edu.pl', 'annam.chrzan@gmail.com', 's.stachowicz@angloville.pl', 'm.debicka@angloville.pl', 'klaudia@target.edu.pl', 'maria@target.edu.pl', 'aleksandra@target.edu.pl', 'biuro@target.edu.pl', 'k.ossolinski@teachersteam.pl', 'andrzej.w@LSLINGUA.com', 'office@edueduonline.pl', 'biuro@worq.pl', 't.kempf@bebetter.pl', 'office@premium-english.pl', 'magda.waksberg@advantage.edu.pl', 'aneta.wysocka-wejda@advantage.edu.pl', 'a.komosa@sufiks.com.pl', 'Tomasz.Kaminski@hello-blackbird.com', 'katarzyna.warszynska@gmail.com', 'biuro@perfektmissingfor.com', 'smyczeks@wp.pl', 'd.zawadzka@puzelek.edu.pl', 'agnieszka@prolang.pl', 'metodyk2@language.com.pl', 'metodyka@language.com.pl', 'metodyk@language.com.pl', 'cosmopolitan@cosmopolitan.edu.pl', 'pelka@beclever.edu.pl', 'zaneta.gasidlo@homecenter.pl', 'espytkowska@ili.edu.pl', 'akieda@ili.edu.pl', 'orka@tlen.pl', 'andre.chmielewski@akademeia.edu.pl', 'kursy@yellow.pl', 'malgorzata.nurek@wp.pl', 'joannatk2@poczta.onet.pl', 'monikagedu@gmail.com', 'angielski@about.me', 'paula.grzeda@gmail.com', 'adarlak@infiniti.pl', 'j.lanski@mindz.it', 'k.ptaszek@mindz.it', 'directorKAT@3lions.pl', 'director@3lions.pl', 'office@edueduonline.pl', 'lingualanddos@gmail.com', 'andrzej.w@LSLINGUA.com', 'advantage@advantage.edu.pl', 'magda.waksberg@advantage.edu.pl', 'davidgasper@rocketmail.com', 'justyna.stapor@enkipolska.com', 'fly@simplito.com', 'biuro@worq.pl', 'office@premium-english.pl', 'ewa.nawrocka@tkinvestments.pl', 'anna@linguasmart.pl', 'emilia@linguasmart.pl', 'czwilliams01@gmail.com', 'beata@lingopro.pl', 'office@inl-gda.com', 'szkola@kskperfekt.pl', 'biuro@languagebox.pl', 'j.wlazlinska@cksource.com', 'praca@bs-ict.pl', 'classes@nativespeaker.edu.pl', 'archerschool@op.pl', 'sp.inspiracja@gmail.com', 'ola@lingopro.pl', 'kwieckiewicz@select-solutions.pl', 'kontakt@1academy.pl', 'agalazka@futurecentre.eu', 'hanna.adamska@erp-recycling.org', 'enterprisecode.bc@gmail.com', 'milena00000@o2.pl', 'piotrchudzik.ipad2@gmail.com', 'biuro@sayhello.com.pl', 'anglevitaewamach@gmail.com', 'metodykaspre@gmail.com', 'eliteenglishsch@gmail.com ', 'Chris_farman@hotmail.com', 'm.machalska90@gmail.com', 'andrzej.pajda@varunit.com', 'katarzyna.idziaszczyk@progres.edu.pl', 'ula@adminotaur.pl', 'office@internationalvillage.eu', 'info@fayno.camp', 'anasteishahordyak@gmail.com', 'kontakt@luckyluke.edu.pl', 'lektor@angielskiwpracy.com.pl', 'andypaul100@gmail.com', 'jezyki@eagle.pl', 'sudyka@liceum.neoschool.pl', 'kaspal@poczta.onet.pl', 'l.sennik@worldwideschool.pl', 'erohe@ability.com.pl', 'izabelaprinz@yahoo.co.uk', 'MTablac@Global-LT.com', 'poliglota@poliglota.pl', 'b.wiktorowska@nanogroup.eu', 'poplawscy@onet.pl', 'native.english.steve@gmail.com', 'piatkowska-m@wp.pl', 'anna.glab@medhub.pl', 'm-valmer@web.de', 'edudzic@ksse.com.pl', 'duke.vu@imeshup.com', 'Duke.vu@risendot.com', 'katarzyna.cieslik@zso5.sosnowiec.pl', 'elemont@elemont.pl', 'dbamoran@gmail.com', 'janusz.komorniczak@mytummy.pl', 'henrywilczewski@yahoo.co.uk', 'e.mrthumbs@gmail.com', 'agata.ziolo@4professionals.pl', 'hr@boyar.info', 'moneypolo.pl@gmail.com', 'championsacademy@epoczta.pl', 't.nitka@bakalarz.pl', 'hgliwa@wp.pl', 'kontakt@beenglish.pl', 'warsawenglishschool1@gmail.com', 'agata.stasiak@ansharstudios.com', 'kontakt@betterleaders.eu', 'Karolina.Janikowska@shi-g.com', 'klaudia.orzechowska@diuna.biz', 'e.kaliszewska@lektor.com.pl', 'kontakt@kokosowa-wyspa.pl', 'biuro@languaged.pl', 'j.witkowski@languaged.pl', 'Joanna Ustowska ju@montessori.sopot.pl', 'agnieszka.rokosz@warta.pl', 'centrum@ambit.strefa.pl', 'agnieszka.groborz@edusmart.com.pl', 'amocek@select-solutions.pl', 'ewa.puliak@gmail.com', 'praca@takeup.pl', 'joannashill@interia.pl', 'biuro@victoriacenter.pl', 'info@hydeparkenglish.com', 'support@ability.com.pl', 'r.sobczak@ventureinc.com', 'incompany@bs-ict.pl', 'k.danek@przestertv.pl', 'info@mousepad-express.com', 'aleksandra@unisystem-displays.com', 'info@advantis.pl', 'Milica.Andic@learnlight.com', 'monikakarkosik@o2.pl', 'magdalena.kowalczyk5@gmail.com', 'beata.halas@gmail.com', 'grzegorz@brandkolektyw.pl', 'b.widawska@gatee.eu', 'agnieszka.korzeniewska@clicktrans.pl', 'tomek@productify.pl', 'tomek@productify.it', 'przedszkole@stumilowy.com', 'henrywolf.london@gmail.com']
    
    return redirect_to(advertise_path) if message_params[:from].in?(blocked_emails) || !verify_hcaptcha
    # silently prevent spam from asdgrrty.com domain
    return redirect_to @profile, notice: 'Message was sent!' if ('asdgrrty.com'.in?(message_params[:from]) || 'theprettyguinea'.in?(message_params[:body]))
    @message = Message.create(message_params)
	  respond_to do |format|
      if @message
        @profile = Profile.find(@message.to_id)
        @profile.waiting_messages += 1
        @profile.save
        format.html { redirect_to @profile, notice: 'Message was sent!' }
      end
    end
  end

  def index
    @messages = @current_profile.messages.order(created_at: :desc)
    @current_profile.waiting_messages = 0
    @current_profile.reminders_sent = 0
    @current_profile.save
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  # Never trust parameters from the scary internet, only allow the white list through.
  def message_params
    params.require(:message).permit(:from, :to_id, :body)
  end
end
