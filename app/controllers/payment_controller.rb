class PaymentController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:receive, :paypal]

	def index
		authorize :payment
	end

  def thankyou
  end

  def receive
    if params["operation_type"]=="payment" && params["operation_status"]=="completed" && params["operation_currency"]==@current_domain.currency
      upgrade_membership_from_invoice_code(params["control"], params["operation_amount"].to_i)
      render nothing: true, status: 200, content_type: 'text/html'
    end
  end

	def paypal
    puts "Paypal payment status: #{params["payment_status"]}"
		if verified_paypal_ipn? && params["payment_status"].in?(["Pending", "Confirmed"]) && params["mc_currency"]==@current_domain.currency
      upgrade_membership_from_invoice_code(params["item_number"], params["mc_gross"].to_i)
      render nothing: true, status: 200, content_type: 'text/html'
    else
      render plain: 'ERROR', status: 500, content_type: 'text/html'
    end
  end

  private

  def upgrade_membership_from_invoice_code(invoice_code, price)
    if invoice_code.split(":")[0]=="premium"
      puts 'premka'
      @profile = Profile.find(invoice_code.split(":")[1])
      previous_time = @profile.premium_until
      @profile.premium_until = (@profile.domain.premium_duration.days/@profile.domain.premium_cost * price).seconds.from_now
      @profile.save
      if (@profile.premium_until - previous_time) > 36000
        ProfileMailer.payment_received_mail(@profile).deliver_later
        @profile.approved!
      else
        logger.info "DotPay is going mad"
      end
    elsif invoice_code.split(":")[0]=="featured"
      puts 'feature'
      @profile = Profile.find(invoice_code.split(":")[1])
      previous_time = @profile.featured_until
      @profile.featured_until = (@profile.domain.featured_duration.days/@profile.domain.featured_cost * price).seconds.from_now
      @profile.featured_visible = true
      @profile.save
      if (@profile.featured_until - previous_time) > 36000
        ProfileMailer.payment_received_mail(@profile).deliver_later
      else
        logger.info "DotPay is going mad"
      end
    elsif invoice_code.split(":")[0]=="advertisement"
      puts 'advert'
      @advertisement = Advertisement.find(invoice_code.split(":")[1])
      @advertisement.paid_until = (@advertisement.domain.advertisement_duration.days/@advertisement.domain.advertisement_cost * price).seconds.from_now
      @advertisement.save
    end
  end

  def verified_paypal_ipn?
    Net::HTTP.get_response(URI.parse("#{ENV['paypal_url']}?cmd=_notify-validate&#{request.raw_post}")).body == "VERIFIED"
  end
end

