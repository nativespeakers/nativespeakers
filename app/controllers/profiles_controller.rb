class ProfilesController < ApplicationController
  before_action :set_profile_and_authorize, only: [:show, :edit, :update, :destroy, :approve, :reject, :upgrade]

  # GET /profiles
  # GET /profiles.json
  def index
    authorize Profile
    @profiles = Profile.all
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
    @message = Message.new
  end

  # GET /profiles/new
  def new
    if current_profile.nil?
      @profile = Profile.new
    else
      @profile = current_profile
      render 'edit'
    end
  end

  # GET /profiles/1/edit
  def edit
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(profile_params.merge(:user_id => current_user.id, :domain_id => current_domain.id ))

    respond_to do |format|
      if @profile.save
        @profile.pending!
        ProfileMailer.waiting_for_approval_mail(@profile).deliver
        format.html { redirect_to upgrade_path, notice: 'Profile was successfully created.' }
        format.json { render :show, status: :created, location: upgrade_path }
      else
        format.html { render :new }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    authorize @profile
    respond_to do |format|
      if @profile.update(profile_params)
        unless @profile.premium?
          @profile.pending! 
          ProfileMailer.waiting_for_approval_mail(@profile).deliver
        end
        format.html { redirect_to upgrade_path, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: upgrade_path }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html { redirect_to profiles_url, notice: 'Profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def search
    @distance = params[:distance]
    @distance = 50 unless @distance.present? && @distance.to_i.between?(2,50)
    @location = params[:location]
    search_conditions
    @profiles = Profile.select("profiles.*, users.last_active_at, CASE WHEN profiles.premium_until IS NULL THEN 0 WHEN profiles.premium_until < '#{Time.now.utc.to_s(:db)}' THEN 0 ELSE 1 END AS premium")
                       .where(search_conditions)
                       .where(domain: current_domain).joins(:languages).joins(:age_groups).joins(:user)
                       .where('languages.name = ?', params[:language])
    @profiles = @profiles.within(@distance, origin: [params[:lat], params[:lng]]) if @location.present?
    @profiles = @profiles.order('premium DESC, users.last_active_at DESC')
                         .distinct
    @profiles = @profiles.paginate(page: params[:page], per_page: (params[:per_page] || 50 ) )
  end

  def show_all
    @profiles = Profile.where(domain: current_domain, status: Profile.statuses[:approved]).paginate(page: params[:page], per_page: (params[:per_page] || 50 ) )
  end

  def feature
    authorize @current_profile
    @current_profile.featured_until = 30.minutes.from_now
    @current_profile.featured_visible = false
    render layout: false
  end

  def dashboard
    authorize Profile
    @pending = Profile.pending
    @rejected = Profile.rejected
    @premium = Profile.premium
    @plain = Profile.plain
  end

  def approve
    if @profile.approved!
      ProfileMailer.approved_mail(Profile.last).deliver
      redirect_to @profile, notice: 'Profile was approved!' 
    end
  end
  
  def reject
    if @profile.rejected!
      ProfileMailer.rejected_mail(Profile.last).deliver
      redirect_to @profile, notice: 'Profile was rejected!' 
    end
  end


  def upgrade
    @profile.premium_until = Date.parse(params[:date])
    @profile.save
    redirect_to @profile, notice: "Profile was upgraded until #{Date.parse(params[:date])}" 
  end

  private

  def search_conditions
    age_group_id = AgeGroup.all.map{|ag| ag.id}
    age_group_id = params[:age_group_id] if params[:age_group_id].present?
    does_online_lessons = [true, false, nil]
    does_online_lessons = true if params[:does_online_lessons]=='yes'
    works_during_weekend = [true, false, nil]
    works_during_weekend = true if params[:works_during_weekends]=='yes'
    issues_an_invoice = [true, false, nil]
    issues_an_invoice = true if params[:issues_an_invoice]=='yes'
    {'age_groups.id': age_group_id, does_online_lessons: does_online_lessons, works_during_weekend: works_during_weekend, issues_an_invoice: issues_an_invoice, status: Profile.statuses[:approved] }
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_profile_and_authorize
    @profile = Profile.find_by(id: params[:id]) || Profile.find_by!(slug: params[:slug], domain: @current_domain)
    authorize @profile
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def profile_params
    params.require(:profile)
          .permit(:name, :telephone, :body, :price, :lat, :lng, :formatted_address,
                  :avatar, :short_description, :public_email, :contact_details,
                  :does_online_lessons, :sex, :works_during_weekend,
                  :issues_an_invoice, :country_id,
                  qualification_ids: [], age_group_ids: [], language_ids: [])
  end
end
