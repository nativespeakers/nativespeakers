class AdvertisementsController < ApplicationController
  before_filter :set_advertisement, only: [:show, :edit, :update, :destroy, :pay]

  def index
    @advertisements = Advertisement.paid
    # @advertisements = Advertisement.all
  end

  def show
  end

  def new
    @advertisement = Advertisement.new
  end

  def edit
    authorize @advertisement
  end

  def create
    @advertisement = Advertisement.new(advertisement_params)
    @advertisement.domain = @current_domain
    if @advertisement.save
      redirect_to pay_advertisement_path(@advertisement), notice: 'Advertisement was successfully saved and is waiting for payment.' 
    else
      render :new
    end
  end

  def update
    authorize @advertisement
    if @advertisement.update(advertisement_params)
      redirect_to @advertisement, notice: 'Advertisement was successfully updated.' 
    else
      render :edit
    end
  end

  def destroy
    if authorize @advertisement, :destroy?
      @advertisement.destroy
      redirect_to advertisements_url, notice: 'Advertisement was successfully destroyed.' 
    else
      redirect_to advertisements_url, notice: 'You don\'t have the access to delete an advert.' 
    end
  end

  def pay
  end

  private

  def advertisement_params
    params.require(:advertisement).permit(:picture, :header, :body, :salary, :lat, :lng, :formatted_address, :qualifications, :email, :this_and_that)
  end

  def set_advertisement
    @advertisement = Advertisement.find(params[:id])
  end

end
