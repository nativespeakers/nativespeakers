class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  require "paperclip/storage/ftp"
  protect_from_forgery with: :exception
  before_filter :current_domain, :current_profile, :set_mailer_domain
  before_filter :record_user_activity
  before_filter :set_raven_context
  include Pundit

 
  protected
  def current_domain
    @current_domain ||= Domain.find_by(url: request.host_with_port) || Domain.find_by(url: "www."+request.host_with_port) || Domain.create(url: request.host_with_port, code: "PL")
  end 
  
  def current_profile
    if current_user.present?
      @current_profile ||= current_user.profiles.where("domain_id = ?", current_domain.id)[0]
    end
  end

  def set_mailer_domain
    ActionMailer::Base.default_url_options = { host: @current_domain.url}
  end
  
  private

  def record_user_activity
    if current_user
      current_user.touch :last_active_at
    end
  end

  def set_raven_context
    Raven.user_context(user_id: current_user.try(:id)) # or anything else in session
    Raven.extra_context(params: params.to_unsafe_h, url: request.url)
  end

  def verify_hcaptcha
    token = params['h-captcha-response']
    parameters = { 
       "secret": "0x79dd7e7a2e156d639961B9b5fd4eB7fe4281E0F9",
       "response": token
    }
    JSON.parse(Faraday.post('https://hcaptcha.com/siteverify', parameters).body)['success']
  end
end
