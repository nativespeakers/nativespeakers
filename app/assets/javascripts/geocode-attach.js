document.addEventListener("turbolinks:load", function() {
   if ($("#location").length) {
     $("#location").geocomplete({
        details: ".location_details",
        detailsAttribute: "data-geo",
        country: $('[data-geo="country_code"]').val()
      });
     $("#location").geocomplete("find", $('[data-geo="formatted_address"]').val());
      
    //   $("#find").click(function(){
    //     $("#geocomplete").trigger("geocode");
    //   });
    // });
  }
});
