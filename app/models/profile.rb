class Profile < ActiveRecord::Base
  enum status: { not_filled_in: 0, pending: 1, rejected: 2, approved: 3 }

  before_create :set_default_values
  before_validation :strip_spaces_in_name, :set_slug

  belongs_to :user
  belongs_to :domain

  validates :short_description, length: { maximum: 180 }
  validates :name, length: {minimum: 5, maximum: 22 }
  validates :price, numericality: { only_integer: true, greater_than: 10, less_than: 300 }
  validates_presence_of :country

  belongs_to :country
  has_and_belongs_to_many :qualifications
  has_and_belongs_to_many :languages
  has_many :messages, foreign_key: "to_id"
  has_and_belongs_to_many :age_groups


  acts_as_mappable :default_units => :kms,
   :default_formula => :flat,
   :distance_field_name => :distance,
   :lat_column_name => :lat,
   :lng_column_name => :lng


  #has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"

  has_attached_file :avatar, {
    styles: { big: "500x500>", medium: "300x300>", thumb: "100x100>" },
    default_url: ->(attachment) { ActionController::Base.helpers.asset_path('male-placeholder-image.jpg') },
  # Choose the FTP storage backend
  :storage => :ftp,

  # Set where to store the file on the FTP server(s).
  # This supports Paperclip::Interpolations.
  :path => Rails.env+"/:attachment/:id/:style/:filename",

  # The full URL of where the attachment is publicly accessible.
  # This supports Paperclip::Interpolations.
  :url => ENV['ftp_public_path']+Rails.env+"/:attachment/:id/:style/:filename",

  # The list of FTP servers to use
  :ftp_servers => [
    {
      :host     => ENV['ftp_host'],
      :user     => ENV['ftp_user'],
      :password => ENV['ftp_password'],
      :passive  => true  # optional, false by default
    }
  ],

  # Optional socket connect timeout (in seconds).
  # This only limits the connection phase, once connected
  # this option is of no more use.
  # :ftp_connect_timeout => 5, # optional, nil by default (OS default timeout)

  # Optional flag to skip dead servers.
  # If set to true and the connection to a particular server cannot be
  # established, the connection error will be ignored and the files will
  # not be uploaded to that server.
  # If set to false and the connection to a particular server cannot be established,
  # a SystemCallError will be raised (Errno::ETIMEDOUT, Errno::ENETUNREACH, etc.).
  :ftp_ignore_failing_connections => false, # optional, false by default

  # Optional flag to keep empty parent directories when deleting files.
  :ftp_keep_empty_directories => true # optional, false by default
}

  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  def premium?
    self.premium_until && !self.premium_until.past?
  end

  def featured?
    self.featured_until && self.featured_visible && !self.featured_until.past?
  end

  def self.premium
    where('premium_until > ?', Time.now.utc.to_s(:db))
  end

  def self.plain
    where('premium_until < ?', Time.now.utc.to_s(:db))
  end

  private
  def set_default_values
    self.premium_until ||= Time.now
    self.featured_until ||= Time.now
    self.waiting_messages ||= 0
    self.reminders_sent ||= 0
    self.last_reminder_sent_at ||= Time.now
  end

  def strip_spaces_in_name
    self.name.strip!
    (self.name.include? " ") ? nil : errors.add(:name, 'Your name should contain at least one space')
  end

  def set_slug
    self.slug = self.name.downcase.to_ascii.gsub(/\s+/, '-').gsub(/[^a-z-]/, "") # First it converts spaces to dashes, then deletes all other characters but ascii and dashes
  end
end
