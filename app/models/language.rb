class Language < ActiveRecord::Base
  
  has_many :qualifications
  has_many :profiles, through: :qualifications
  
end
