class Advertisement < ActiveRecord::Base
  belongs_to :domain
  acts_as_mappable :default_units => :kms,
   :default_formula => :flat,
   :distance_field_name => :distance,
   :lat_column_name => :lat,
   :lng_column_name => :lng

  has_attached_file :picture, {
    styles: { large: "800x450>", medium: "400x225>" },
    default_url: "https://i.imgur.com/lKCAXXf.jpg",
    :storage => :ftp,
    :path => Rails.env+"/:attachment/:id/:style/:filename",
    :url => ENV['ftp_public_path']+Rails.env+"/:attachment/:id/:style/:filename",
    :ftp_servers => [{host: ENV['ftp_host'], user: ENV['ftp_user'], password: ENV['ftp_password'], passive: true }],
    :ftp_ignore_failing_connections => false,
    :ftp_keep_empty_directories => true
  }
  validates_attachment_content_type :picture, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  def self.paid
    where('paid_until > ?', Time.now.utc.to_s(:db))
  end
end
