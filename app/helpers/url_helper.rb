module UrlHelper
  def profile_path(profile, options = {})
    if profile.nil?
      "#"
    else
      "/lektor/#{profile.slug}"
    end
  end
end
