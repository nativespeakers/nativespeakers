module ApplicationHelper
  def bootstrap_class_for flash_type
    case flash_type
      when "success"
        "success"
      when "error"
        "danger"
      when "alert"
        "warning"
      when 'notice'
        "info"
      else
        flash_type.to_s
    end
  end


  def project_name
    @project_name ||= @current_domain.name || "elo" 
  end

  def title(page_title, options = {})
    options[:full_title] ? (content_for :title, page_title.to_s) : (content_for :title, page_title.to_s+" | "+project_name) 
  end

  def description(page_description)
    content_for :description, page_description
  end

end
