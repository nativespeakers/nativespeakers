module ProfilesHelper
  def flag_tag(profile)
    image_tag "//flagpedia.net/data/flags/mini/#{profile.country.code.downcase}.png"
  end
end
