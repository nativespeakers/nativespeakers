json.array!(@profiles) do |profile|
  json.extract! profile, :id, :name, :telephone, :price, :user_id
  json.url profile_url(profile, format: :json)
end
