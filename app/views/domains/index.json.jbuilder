json.array!(@domains) do |domain|
  json.extract! domain, :id, :code, :url, :name
  json.url domain_url(domain, format: :json)
end
