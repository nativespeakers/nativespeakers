Rails.application.routes.draw do
  devise_for :users
  resources :profiles do
    member do
      get 'approve'
      get 'upgrade/:date' => 'profiles#upgrade', as: 'upgrade'
      get 'reject'
    end
    collection do
      get 'search' => 'profiles#search'
      get 'show_all' => 'profiles#show_all'
      authenticate :user do
        get 'feature' => 'profiles#feature', as: "new_featured_advert"
        get 'dashboard' => 'profiles#dashboard'
      end
    end
  end

  resources :advertisements do
    member do
      get 'pay'
    end
  end

  get '/lektor/:slug', to: 'profiles#show'
  get '/lektor/:slug/upgrade/:date' => 'profiles#upgrade'
  patch '/lektor/:slug', to: 'profiles#update'

  resources :domains
  
  root 'static_pages#home'

  get 'tos' => 'static_pages#tos'
  get 'about' => 'static_pages#about'
  get 'cookie-policy' => 'static_pages#cookie_policy'

  get 'static_pages/help'
  get 'advertise' => 'static_pages#too_many_messages', as: 'advertise'
  resources :messages, only: [:create]
  authenticate :user do
    resources :messages, except: [:create]
    get 'upgrade' => 'payment#index'
    get 'thank-you' => 'payment#thankyou', as: "thankyou"
  end
  
  post 'payments/receive' => 'payment#receive'
  post 'payments/paypal' => 'payment#paypal'


  # Redirects for SEO searches
  get '/pl/lektor/:slug', to: 'profiles#show'
  get 'native-speaker-warszawa' => 'profiles#search', defaults: { location: 'Warszawa', language: 'English', lat: '52.2296756', lng: '21.012228700000037' }
  get 'native-speaker-wroclaw' => 'profiles#search', defaults: { location: 'Wrocław', language: 'English', lat: '51.1078852', lng: '17.03853760000004' }
  get 'native-speaker-katowice' => 'profiles#search', defaults: { location: 'Katowice', language: 'English', lat: '50.26489189999999', lng: '19.02378150000004' }
  get 'native-speaker-poznan' => 'profiles#search', defaults: { location: 'Poznań', language: 'English', lat: '52.406374', lng: '16.925168100000064' }
  get 'native-speaker-trojmiasto' => 'profiles#search', defaults: { location: 'Trójmiasto: Gdańsk, Gdynia, Sopot', language: 'English', lat: '54.35202520000001', lng: '18.64663840000003' }
  get 'native-speaker-krakow' => 'profiles#search', defaults: { location: 'Kraków', language: 'English', lat: '50.06465009999999', lng: '19.94497990000002' }
  get 'native-speaker-szczecin' => 'profiles#search', defaults: { location: 'Szczecin', language: 'English', lat: '53.4285438', lng: '14.552811600000041' }
  get 'native-speaker-lodz' => 'profiles#search', defaults: { location: 'Łódź', language: 'English', lat: '51.7592485', lng: '19.45598330000007' }
  get 'native-speaker-lublin' => 'profiles#search', defaults: { location: 'Lublin', language: 'English', lat: '51.2464536', lng: '22.568446300000005' }
  get 'native-speaker-bydgoszcz' => 'profiles#search', defaults: { location: 'Bydgoszcz', language: 'English', lat: '53.12348040000001', lng: '18.008437800000024' }
  get 'native-speaker-torun' => 'profiles#search', defaults: { location: 'Toruń', language: 'English', lat: '53.0137902', lng: '18.59844369999996' }
  get 'native-speaker-czestochowa' => 'profiles#search', defaults: { location: 'Częstochowa', language: 'English', lat: '50.8118195', lng: '19.120309399999996' }



  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
