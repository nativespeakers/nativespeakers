namespace :background do
  desc "TODO"
  task send_reminders: :environment do
    Profile.where('waiting_messages > ? AND reminders_sent < ?', 0, 3).each do |profile|
      ProfileMailer.reminder_mail(profile).deliver_now
    end
  end

  task request_feedback: :environment do
    Profile.where('created_at < ? AND created_at > ?', 14.days.ago, 15.days.ago).each do |profile|
      ProfileMailer.request_feedback_mail(profile).deliver_now
    end
  end
end
