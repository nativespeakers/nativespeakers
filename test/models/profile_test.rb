require 'test_helper'

class ProfileTest < ActiveSupport::TestCase
  test "Should not create a profile with non-existant user" do
    Profile.create(domain_id: 1, user_id: 1)
    assert true
  end
end
