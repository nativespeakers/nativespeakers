class CreateQualifications < ActiveRecord::Migration
  def change
    create_table :qualifications do |t|
      t.string :level
      t.references :language, index: true, foreign_key: true, null: false
      t.references :profile, index: true, foreign_key: true, null: false
    end
  end
end
