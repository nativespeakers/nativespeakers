class AddAdvertisementCostToDomain < ActiveRecord::Migration
  def change
    add_column :domains, :advertisement_cost, :integer
    add_column :domains, :advertisement_duration, :integer
  end
end
