class AddFeaturedFieldsToProfile < ActiveRecord::Migration
  def change
  	add_column :profiles, :featured_until, :datetime
  	add_column :profiles, :featured_visible, :boolean
  end
end
