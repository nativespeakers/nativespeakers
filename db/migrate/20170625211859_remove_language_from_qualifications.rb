class RemoveLanguageFromQualifications < ActiveRecord::Migration
  def change
    remove_reference :qualifications, :language, index: true, foreign_key: true
  end
end
