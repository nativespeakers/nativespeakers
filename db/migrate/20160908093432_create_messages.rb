class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :from
      t.references :to, references: :profiles, index: true, foreign_key: true
      t.text :body

      t.timestamps null: false
    end
  end
end
