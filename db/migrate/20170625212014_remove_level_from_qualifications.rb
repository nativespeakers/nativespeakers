class RemoveLevelFromQualifications < ActiveRecord::Migration
  def change
    remove_column :qualifications, :level, :string
  end
end
