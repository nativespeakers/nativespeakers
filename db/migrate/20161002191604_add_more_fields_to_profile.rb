class AddMoreFieldsToProfile < ActiveRecord::Migration
  def change
  	add_column :profiles, :sex, :string, :limit => 1
  	#https://en.wikipedia.org/wiki/ISO/IEC_5218

  	create_table :age_groups do |t|
  	  t.string :name
  	end

    create_table :age_groups_profiles, id: false do |t|
      t.belongs_to :profile, index: true
      t.belongs_to :age_group, index: true
    end

    create_table :levels do |t|
  	  t.string :name
  	end

  	create_table :levels_profiles, id: false do |t|
      t.belongs_to :profile, index: true
      t.belongs_to :level, index: true
    end



  end
end
