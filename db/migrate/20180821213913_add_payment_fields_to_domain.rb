class AddPaymentFieldsToDomain < ActiveRecord::Migration
  def change
    add_column :domains, :payment_gateway, :string
    add_column :domains, :currency, :string
    add_column :domains, :premium_cost, :integer
    add_column :domains, :premium_duration, :integer
    add_column :domains, :featured_cost, :integer
    add_column :domains, :featured_duration, :integer
  end
end
