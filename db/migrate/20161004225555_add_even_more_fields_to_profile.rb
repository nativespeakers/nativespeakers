class AddEvenMoreFieldsToProfile < ActiveRecord::Migration
  def change
  	add_column :profiles, :works_during_weekend, :boolean
  	add_column :profiles, :issues_an_invoice, :boolean
  end
end
