class AddEmailToAdvertisements < ActiveRecord::Migration
  def change
    add_column :advertisements, :email, :string
  end
end
