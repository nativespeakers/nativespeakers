class DropLevels < ActiveRecord::Migration
  def change
    drop_table :levels
  	drop_table :levels_profiles
  end
end
