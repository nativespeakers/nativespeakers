class AddInfoColumnsToAdvertisement < ActiveRecord::Migration
  def change
    add_column :advertisements, :qualifications, :text
    add_column :advertisements, :this_and_that, :text
  end
end
