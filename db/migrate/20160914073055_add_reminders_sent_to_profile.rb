class AddRemindersSentToProfile < ActiveRecord::Migration
  def change
  	add_column :profiles, :reminders_sent, :integer
  end
end
