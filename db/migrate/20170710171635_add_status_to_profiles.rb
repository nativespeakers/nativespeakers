class AddStatusToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :status, :integer, default: 0
  end
end
