class AddPublicEmailToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :public_email, :string
  end
end
