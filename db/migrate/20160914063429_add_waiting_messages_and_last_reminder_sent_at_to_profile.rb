class AddWaitingMessagesAndLastReminderSentAtToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :waiting_messages, :integer
    add_column :profiles, :last_reminder_sent_at, :datetime
  end
end
