class AddCountryToProfiles < ActiveRecord::Migration
  def change
    add_reference :profiles, :country, index: true, foreign_key: true
  end
end
