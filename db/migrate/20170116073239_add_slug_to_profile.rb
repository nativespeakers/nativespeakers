class AddSlugToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :slug, :text, :null => false, :default => true
    add_index "profiles", ["domain_id", "slug"], :unique => true
  end
end
