class AddDoesOnlineLessonsToProfile < ActiveRecord::Migration
  def change
  	    add_column :profiles, :does_online_lessons, :boolean
  end
end
