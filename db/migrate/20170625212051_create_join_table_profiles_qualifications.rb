class CreateJoinTableProfilesQualifications < ActiveRecord::Migration
  def change
    create_join_table :profiles, :qualifications do |t|
      # t.index [:profile_id, :qualification_id]
      # t.index [:qualification_id, :profile_id]
    end
  end
end
