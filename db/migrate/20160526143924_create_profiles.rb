class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :telephone
      t.integer :price
      t.references :user, null: false, index: true, foreign_key: true
      t.references :domain, null: false, index: true, foreign_key: true
      t.timestamps null: false
      t.index([:user_id, :domain_id], unique: true)
    end
  end
end
