class CreateAdvertisements < ActiveRecord::Migration
  def change
    create_table :advertisements do |t|
      t.has_attached_file :picture
      t.text :header
      t.text :body
      t.string :edit_token
      t.integer :salary
      t.references :domain, index: true, foreign_key: true
      t.float :lat
      t.float :lng
      t.text :formatted_address
      t.datetime :paid_until
    end
  end
end
