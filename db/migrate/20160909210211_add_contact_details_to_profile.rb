class AddContactDetailsToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :contact_details, :text
  end
end
