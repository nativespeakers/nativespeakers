class AddFormattedAddressToProfile < ActiveRecord::Migration

  def change
  	add_column :profiles, :formatted_address, :text
  end
end
