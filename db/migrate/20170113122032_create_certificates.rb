class CreateCertificates < ActiveRecord::Migration
  def change

    create_table :certificates do |t|
      t.string :name
    end

    create_table :certificates_profiles, id: false do |t|
      t.belongs_to :profile, index: true
      t.belongs_to :certificate, index: true
    end


  end
end
