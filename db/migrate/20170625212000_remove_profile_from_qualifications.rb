class RemoveProfileFromQualifications < ActiveRecord::Migration
  def change
    remove_reference :qualifications, :profile, index: true, foreign_key: true
  end
end
