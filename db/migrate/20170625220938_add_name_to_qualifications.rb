class AddNameToQualifications < ActiveRecord::Migration
  def change
    add_column :qualifications, :name, :string
  end
end
