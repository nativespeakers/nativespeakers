class CreateDegrees < ActiveRecord::Migration
  def change
    create_table :degrees do |t|
      t.string :name
    end

    create_table :degrees_profiles, id: false do |t|
      t.belongs_to :profile, index: true
      t.belongs_to :degree, index: true
    end
  end
end
