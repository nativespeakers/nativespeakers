class AddPremiumUntilToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :premium_until, :datetime
    change_column_default :profiles, :premium_until, 'NOW()'
  end
end
