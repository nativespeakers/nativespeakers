class CreateJoinTableProfilesLanguages < ActiveRecord::Migration
  def change
    create_join_table :profiles, :languages do |t|
      # t.index [:profile_id, :language_id]
      # t.index [:language_id, :profile_id]
    end
  end
end
