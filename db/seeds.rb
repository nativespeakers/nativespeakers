load(Rails.root.join( 'db', 'seeds', "#{Rails.env.downcase}.rb"))

united_states = Country.create(name: "United States", code: "US", emoji: "🇺🇸")
united_kingdom = Country.create(name: "United Kingdom", code: "gb", emoji: "🇬🇧")

admin_user = User.create(email: "nativespeaker.com.pl+admin@gmail.com", password: "testtest", admin: true, moderator: false, confirmed_at: Time.now)
Profile.create("name"=>"Mr Administrator", "user_id" => admin_user.id, "domain_id" => @current_domain.id, "telephone"=>"796 548 647", "price"=>"20", "body"=>"Serdecznie zapraszam\r\n", lat: 50.3072757, lng: 18.677715900000067, formatted_address: "Warszawska, Gliwice, Poland", avatar: File.new("#{Rails.root}/public/seed_images/admin.jpg", "r"), short_description: "Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don't simply skip over it entirely.", premium_until: 1.year.ago, country: united_states)

moderator_user = User.create(email: "nativespeaker.com.pl+moderator@gmail.com", password: "testtest", admin: false, moderator: true, confirmed_at: Time.now)
Profile.create("name"=>"Mr Moderator", "user_id" => moderator_user.id, "domain_id" => @current_domain.id, "telephone"=>"796 548 647", "price"=>"20", "body"=>"Serdecznie zapraszam\r\n", lat: 50.3249278, lng: 18.785718599999996, formatted_address: "Zabrze, Poland", avatar: File.new("#{Rails.root}/public/seed_images/moderator.jpg", "r"), short_description: "“Live as if you were to die tomorrow. Learn as if you were to live forever.”", premium_until: 1.year.from_now, featured_until: 1.year.from_now, featured_visible: true, public_email: "dk@zso10.gliwice.pl", country: united_kingdom)


plain_user = User.create(email: "nativespeaker.com.pl+plain@gmail.com", password: "testtest", admin: false, moderator: false, confirmed_at: Time.now)
Profile.create("name"=>"Mr Plain", "user_id" => plain_user.id, "domain_id" => @current_domain.id, "telephone"=>"796 548 647", "price"=>"20", "body"=>"Serdecznie zapraszam\r\n", lat: 52.2296756, lng: 21.012228700000037, formatted_address: "Warsaw, Poland", avatar: File.new("#{Rails.root}/public/seed_images/plain.png", "r"), short_description: "“The best way to find yourself is to lose yourself in the service of others.”", premium_until: 1.year.ago, featured_until: 1.year.from_now, featured_visible: true, country: united_states)


Language.create(name: "English")
Language.create(name: "Spanish")

['Children (up to 12 years old)', 'Teenagers (13-17 years old)', 'Students (18-26 years old)', 'Adults (26 years old and up)'].each do |ag|
	AgeGroup.create(name: ag)
end

['TESOL', 'CELTA', 'TEFL/TESL', 'DELTA', 'TESOL Diploma'].each do |c|
	Qualification.create(name: c)
end

["Bachelor's degree (BA, MS, BEd, BFA, etc.)",
"Master's degree (MA, MS, MEd, MFA, etc.)",
"Doctorate (PhD, EdD, etc.)",
"Business/Accounting degree (MBA, DBA, etc.)",
"Legal degree (LLB, JD, etc.)",
"Medical degree (MD, DDM, DVM, etc.)"].each do |d|
	Qualification.create(name: d)
end
