# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180826171452) do

  create_table "advertisements", force: :cascade do |t|
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.text     "header"
    t.text     "body"
    t.string   "edit_token"
    t.integer  "salary"
    t.integer  "domain_id"
    t.float    "lat"
    t.float    "lng"
    t.text     "formatted_address"
    t.datetime "paid_until"
    t.text     "qualifications"
    t.text     "this_and_that"
    t.string   "email"
  end

  add_index "advertisements", ["domain_id"], name: "index_advertisements_on_domain_id"

  create_table "age_groups", force: :cascade do |t|
    t.string "name"
  end

  create_table "age_groups_profiles", id: false, force: :cascade do |t|
    t.integer "profile_id"
    t.integer "age_group_id"
  end

  add_index "age_groups_profiles", ["age_group_id"], name: "index_age_groups_profiles_on_age_group_id"
  add_index "age_groups_profiles", ["profile_id"], name: "index_age_groups_profiles_on_profile_id"

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.string   "emoji"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "domains", force: :cascade do |t|
    t.string   "code"
    t.string   "url"
    t.string   "name"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "header"
    t.string   "payment_gateway"
    t.string   "currency"
    t.integer  "premium_cost"
    t.integer  "premium_duration"
    t.integer  "featured_cost"
    t.integer  "featured_duration"
    t.integer  "advertisement_cost"
    t.integer  "advertisement_duration"
  end

  create_table "languages", force: :cascade do |t|
    t.string "name"
  end

  create_table "languages_profiles", id: false, force: :cascade do |t|
    t.integer "profile_id",  null: false
    t.integer "language_id", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.string   "from"
    t.integer  "to_id"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "messages", ["to_id"], name: "index_messages_on_to_id"

  create_table "profiles", force: :cascade do |t|
    t.string   "name"
    t.string   "telephone"
    t.integer  "price"
    t.integer  "user_id",                                       null: false
    t.integer  "domain_id",                                     null: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.text     "body"
    t.float    "lat"
    t.float    "lng"
    t.text     "formatted_address"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "short_description"
    t.string   "public_email"
    t.datetime "premium_until"
    t.text     "contact_details"
    t.boolean  "does_online_lessons"
    t.integer  "waiting_messages"
    t.datetime "last_reminder_sent_at"
    t.integer  "reminders_sent"
    t.datetime "featured_until"
    t.boolean  "featured_visible"
    t.string   "sex",                   limit: 1
    t.boolean  "works_during_weekend"
    t.boolean  "issues_an_invoice"
    t.text     "slug",                            default: "t", null: false
    t.integer  "country_id"
    t.integer  "status",                          default: 0
  end

  add_index "profiles", ["country_id"], name: "index_profiles_on_country_id"
  add_index "profiles", ["domain_id", "slug"], name: "index_profiles_on_domain_id_and_slug", unique: true
  add_index "profiles", ["domain_id"], name: "index_profiles_on_domain_id"
  add_index "profiles", ["lat", "lng"], name: "index_profiles_on_lat_and_lng"
  add_index "profiles", ["user_id", "domain_id"], name: "index_profiles_on_user_id_and_domain_id", unique: true
  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id"

  create_table "profiles_qualifications", id: false, force: :cascade do |t|
    t.integer "profile_id",       null: false
    t.integer "qualification_id", null: false
  end

  create_table "qualifications", force: :cascade do |t|
    t.string "name"
  end

  create_table "users", force: :cascade do |t|
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.boolean  "admin"
    t.boolean  "moderator"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "last_active_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
